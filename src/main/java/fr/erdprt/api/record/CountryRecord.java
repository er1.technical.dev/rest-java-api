package fr.erdprt.api.record;

import java.util.List;

public record CountryRecord(Integer id, String name, String iso, String niceName, String iso3, Short numCode, Short phoneCode, List<UserRecord> users) {
	
	public CountryRecord(Integer id, String name, String iso, String niceName, String iso3, Short numCode, Short phoneCode) {
		this(id, name, iso, niceName, iso3, numCode, phoneCode, null);
	}
}
