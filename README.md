[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=er1.technical.dev_rest-java-api&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=er1.technical.dev_rest-java-api)

Rest Application sample

spring boot sample
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Actuator](#2-actuator)
  1. [Urls](#21-urls)

# 1. Introduction

Run with:
```bash
mvn clean spring-boot:run
```

Access to :
```bash
http://localhost:8080/rest-java-api/api/name/
```

## 1.1. endpoints

### 1.1.1 console h2
http://localhost:8080/rest-java-api/h2-console


### 1.1.2 countries
http://localhost:8080/rest-java-api/h2-console

# 2. Actuator

## 21. Urls

health:
```bash
http://localhost:8080/rest-java-api/actuator/health
```

info:
```bash
http://localhost:8080/rest-java-api/actuator/info
```

metrics:
```bash
http://localhost:8080/rest-java-api/actuator/metrics
```

loggers:
```bash
http://localhost:8080/rest-java-api/actuator/loggers
```

# 3. Docker

## 31. Build

```bash
docker build -t rest-java-api .
```


## 32. Run

```bash
docker run -d -p 8080:8080 --name rest-java-api rest-java-api
```

## 33. pull from gitlab registry

```bash
docker pull registry.gitlab.com/erdprt/rest-java-api:dev-rest-java-api-1.0.0-SNAPSHOT
```

# 4. SonarCloud

## 41. Access

```bash
https://sonarcloud.io/project/overview?id=erdprt_rest-java-api
```

# 5. launch

## 51. run test

2 existing profiles: dev and int

```bash
mvn clean test -Pdev
```

To run (default dev/h2)
```bash
mvn clean spring-boot:run 
```

To run (int/postgresql)
```bash
mvn clean spring-boot:run -Pint 
```


# 6. Documentation (swagger)

## 61. Access

```bash
http://localhost:8080/rest-java-api/swagger-ui/index.html```

# 7. miscellaneous

## 71. grafana/loki

Run docker-compose for loki
```bash
docker-compose -f D:\JAVA\WS\GITLAB_2_WS\rest-java-api\src\main\docker\docker-compose-loki.yml up -d
```
We can access to grafana via 
```bash
http://localhost:3000/
```

Go to src/main/resources/logback.xml
Uncomment the part: 
```bash
<appender name="LOKI" class="com.github.loki4j.logback.Loki4jAppender">
```

on grafana console:
	- Go to Explore
	- Label filters: level = DEBUG (for instance)
	- Run query
	- we can add to Dashboard
	
# 7. Miscellaneous
Using DVD Rental ER Model

https://neon.tech/postgresql/postgresql-getting-started

