package fr.erdprt.api.record;

import java.sql.Timestamp;

public record CategoryRecord(Integer id, String name, Timestamp lastUpdate) {

}
