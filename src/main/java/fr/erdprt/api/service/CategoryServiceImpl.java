package fr.erdprt.api.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fr.erdprt.api.model.Category;
import fr.erdprt.api.repository.CategoryRepository;

@CacheConfig(cacheNames = { "category" })
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	public static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	@Cacheable()
	public List<Category> findAll() {

		List<Category> categories = this.categoryRepository.findAll();
		return categories;
	}

	@Override
	@Cacheable(value = "category", key = "#id")
	public Category findById(final Integer id) {

		Optional<Category> category = this.categoryRepository.findById(id);
		if (!category.isEmpty()) {
			return category.get();
		}
		return null;
	}

}
