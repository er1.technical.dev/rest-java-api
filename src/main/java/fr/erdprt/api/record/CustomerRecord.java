package fr.erdprt.api.record;

import java.sql.Timestamp;
import java.util.List;

public record CustomerRecord(Integer id, StoreRecord store, List<PaymentRecord> payments, String firstName,
		String lastName, String email, AddressRecord address, Boolean activebool, Short active, Timestamp creationDate,
		Timestamp lastUpdate) {

}
