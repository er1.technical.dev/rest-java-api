package fr.erdprt.api.service;

import java.util.List;

import fr.erdprt.api.model.Country;
import fr.erdprt.api.record.CountryRecord;

public interface CountryService {

	List<CountryRecord> findAll();
	
	List<CountryRecord> like(final String name);

	CountryRecord findById(Integer id);

	CountryRecord save(CountryRecord country);

}