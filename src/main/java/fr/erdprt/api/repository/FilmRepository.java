package fr.erdprt.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.erdprt.api.model.Film;

public interface FilmRepository extends JpaRepository<Film, Integer> {

	@Query("select film from Film film LEFT JOIN FETCH film.actors LEFT JOIN FETCH film.category LEFT JOIN FETCH film.language language")
	public List<Film> findAllEager();

}
