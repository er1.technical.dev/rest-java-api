package fr.erdprt.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication(scanBasePackages = { "fr.erdprt.api" }) // same as @Configuration @EnableAutoConfiguration
																// @ComponentScan combined
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
