package fr.erdprt.api.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.erdprt.api.model.Inventory;
import fr.erdprt.api.repository.InventoryRepository;
import jakarta.transaction.Transactional;

@CacheConfig(cacheNames = { "inventory" })
@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService {

	public static final Logger logger = LoggerFactory.getLogger(InventoryServiceImpl.class);

	@Autowired
	private InventoryRepository inventoryRepository;

	@Override
	@Cacheable()
	public List<Inventory> findAllEager() {

		List<Inventory> inventories = this.inventoryRepository.findAllEager();
		return inventories;
	}

	@Override
	@Transactional
	public List<Inventory> findByPages(final Integer index, final Integer size) {

		Pageable pageRequest = PageRequest.of(index, size, Sort.by("id").ascending());
		Page<Inventory> page = this.inventoryRepository.findAll(pageRequest);
		return page.getContent();
	}

	@Override
	@Cacheable(value = "inventory", key = "#id")
	@Transactional
	public Inventory findById(final Integer id) {

		Optional<Inventory> inventory = this.inventoryRepository.findById(id);
		if (!inventory.isEmpty()) {
			return inventory.get();
		}
		return null;

		// return this.inventoryRepository.findById(id)
	}

}
