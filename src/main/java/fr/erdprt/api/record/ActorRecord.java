package fr.erdprt.api.record;

import java.sql.Timestamp;

public record ActorRecord(Integer id, String firstName, String lastName, Timestamp lastUpdate) {

}
