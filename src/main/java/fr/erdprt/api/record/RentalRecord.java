package fr.erdprt.api.record;

import java.sql.Timestamp;
import java.util.List;

public record RentalRecord(Integer id, Timestamp rentalDate, List<PaymentRecord> payments, CustomerRecord customer,
		Timestamp returnDate, Timestamp lastUpdate) {

}
