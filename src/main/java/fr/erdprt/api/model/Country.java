package fr.erdprt.api.model;

import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Table(name = "countries")
@Entity
public class Country {

	@Column(name = "id", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "countries_seq")
	@SequenceGenerator(name = "countries_seq", sequenceName = "countries_seq", initialValue = 5, allocationSize = 1)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "iso")
	private String iso;

	@Column(name = "nicename")
	private String niceName;

	@Column(name = "iso3")
	private String iso3;

	@Column(name = "numcode")
	private Short numCode;

	@Column(name = "phonecode")
	private Short phoneCode;

	@JsonManagedReference
	@OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
	private List<User> users;

	public Country() {
		super();
	}

	public Country(String name, String iso, String niceName, String iso3, Short numCode, Short phoneCode) {
		super();
		this.name = name;
		this.iso = iso;
		this.niceName = niceName;
		this.iso3 = iso3;
		this.numCode = numCode;
		this.phoneCode = phoneCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public String getNiceName() {
		return niceName;
	}

	public void setNiceName(String niceName) {
		this.niceName = niceName;
	}

	public String getIso3() {
		return iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	public Short getNumCode() {
		return numCode;
	}

	public void setNumCode(Short numCode) {
		this.numCode = numCode;
	}

	public Short getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(Short phoneCode) {
		this.phoneCode = phoneCode;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		Country other = (Country) obj;
		return Objects.equals(id, other.id) && Objects.equals(iso, other.iso) && Objects.equals(iso3, other.iso3)
				&& Objects.equals(name, other.name) && Objects.equals(niceName, other.niceName)
				&& Objects.equals(numCode, other.numCode) && Objects.equals(phoneCode, other.phoneCode)
				&& Objects.equals(users, other.users);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, iso, iso3, name, niceName, numCode, phoneCode, users);
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + ", iso=" + iso + ", niceName=" + niceName + ", iso3=" + iso3
				+ ", numCode=" + numCode + ", phoneCode=" + phoneCode + "]";
	}
}
