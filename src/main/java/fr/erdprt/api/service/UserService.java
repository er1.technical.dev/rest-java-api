package fr.erdprt.api.service;

import java.util.List;

import fr.erdprt.api.model.User;

public interface UserService {

	List<User> findAll();

	User findById(Integer id);

	User save(User country);

}