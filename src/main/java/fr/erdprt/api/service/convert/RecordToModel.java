package fr.erdprt.api.service.convert;

import fr.erdprt.api.model.Country;
import fr.erdprt.api.record.CountryRecord;

public class RecordToModel {

	
	public static Country convertCountryRecord(CountryRecord countryRecord) {
		Country	country =	new Country(countryRecord.name(), countryRecord.iso(), countryRecord.niceName(), countryRecord.iso3(), countryRecord.numCode(), countryRecord.phoneCode());
		country.setId(countryRecord.id());
		return country;
	}
}
