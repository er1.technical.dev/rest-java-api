package fr.erdprt.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fr.erdprt.api.model.Country;
import fr.erdprt.api.record.CountryRecord;
import fr.erdprt.api.repository.CountryRepository;
import fr.erdprt.api.service.convert.ModelToRecord;
import fr.erdprt.api.service.convert.RecordToModel;

@CacheConfig(cacheNames={"country"})
@Service("countryService")
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;


	@Override
	@Cacheable()
	public List<CountryRecord> findAll() {

		List<Country> countries = this.countryRepository.findAlls();
		return ModelToRecord.convertCountries(countries);
	}

	@Override
	@Cacheable()
	public List<CountryRecord> like(final String name) {

		List<Country> countries =	this.countryRepository.likeIgnoreCase(name);
		return ModelToRecord.convertCountries(countries);
	}

	@Cacheable(value="country", key="#id")
	@Override
	public CountryRecord findById(final Integer id) {
		Optional<Country> country = this.countryRepository.findById(id);
		if (!country.isEmpty()) {
			return ModelToRecord.convertCountry(country.get());
		}
		return null;
	}


	@Cacheable(value="country", key="#id")
	@Override
	public CountryRecord save(final CountryRecord countryRecord) {
		Country country	=	RecordToModel.convertCountryRecord(countryRecord);
		country	=	this.countryRepository.save(country);
		return ModelToRecord.convertCountry(country);

	}
}
