package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.api.mapper.InventoryMapper;
import fr.erdprt.api.model.Inventory;
import fr.erdprt.api.record.InventoryRecord;
import fr.erdprt.api.service.InventoryService;
import fr.erdprt.api.util.CustomErrorType;

@RestController
@RequestMapping(value = "/apis/inventories", produces = MediaType.APPLICATION_JSON_VALUE)
public class InventoryController {

	public static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

	@Autowired
	InventoryService inventoryService;

	@GetMapping(value = "/all/")
	public ResponseEntity<List<InventoryRecord>> findAll() {

		logger.info("Fetching all inventories");
		List<Inventory> inventories = inventoryService.findAllEager();
		List<InventoryRecord> inventoryRecords = InventoryMapper.INSTANCE.inventoryToInventoryRecord(inventories);

		return new ResponseEntity<>(inventoryRecords, HttpStatus.OK);
	}

	@GetMapping(value = "/page")
	public ResponseEntity<List<InventoryRecord>> findPage(
			@RequestParam(name = "index", defaultValue = "0") Integer index,
			@RequestParam(name = "size", defaultValue = "1") Integer size) {

		logger.info("Fetching page inventories");
		List<Inventory> inventories = inventoryService.findByPages(index, size);
		List<InventoryRecord> inventoryRecords = InventoryMapper.INSTANCE.inventoryToInventoryRecord(inventories);

		return new ResponseEntity<>(inventoryRecords, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// country------------------------------------------

	@GetMapping(value = "/id/{id}/")
	public ResponseEntity<InventoryRecord> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching inventory with id {}", id);

		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}

		Inventory inventory = inventoryService.findById(id);

		if (inventory == null) {
			logger.error("Film with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("inventory with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		InventoryRecord inventoryRecord = InventoryMapper.INSTANCE.inventoryToInventoryRecord(inventory);

		return new ResponseEntity<>(inventoryRecord, HttpStatus.OK);
	}

}
