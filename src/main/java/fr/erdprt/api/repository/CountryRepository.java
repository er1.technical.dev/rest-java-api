package fr.erdprt.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.erdprt.api.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
	
	@Query("select country from Country country LEFT JOIN FETCH country.users")
	public List<Country> findAlls();
	
	
	@Query("select country from Country country LEFT JOIN FETCH country.users where lower(country.name) LIKE lower(concat('%', :name,'%'))")
	public List<Country> likeIgnoreCase(@Param("name") String name);

}
