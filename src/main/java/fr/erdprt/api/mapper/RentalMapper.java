package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Rental;
import fr.erdprt.api.record.RentalRecord;

@Mapper(componentModel = "spring")
public interface RentalMapper {

	RentalMapper INSTANCE = Mappers.getMapper(RentalMapper.class);

	RentalRecord rentalToRentalRecord(Rental rental);

	List<RentalRecord> rentalToRentalRecord(List<Rental> rentals);

	Rental rentalRecordToRental(RentalRecord rentalRecord);

	List<Rental> rentalRecordToRental(List<RentalRecord> rentalRecords);

}
