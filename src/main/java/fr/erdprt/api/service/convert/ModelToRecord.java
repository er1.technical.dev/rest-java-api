package fr.erdprt.api.service.convert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.erdprt.api.model.Country;
import fr.erdprt.api.model.User;
import fr.erdprt.api.record.CountryRecord;
import fr.erdprt.api.record.UserRecord;

public class ModelToRecord {


	public static List<CountryRecord> convertCountries(final List<Country> countries) {
		List<CountryRecord> countriesRecord	=	countries.stream().map(country -> convertCountry(country)).collect(Collectors.toList());
		return countriesRecord;
	}

	public static CountryRecord convertCountry(final Country country) {
		CountryRecord countryRecord	=	new CountryRecord(country.getId(), country.getName(), country.getIso(), country.getNiceName(),country.getIso3(), country.getNumCode(), country.getPhoneCode(), convertUsers(country.getUsers()));
		return countryRecord;
	}

	public static List<UserRecord> convertUsers(final List<User> users) {
		List<UserRecord> userRecords	=	users.stream().map(user -> convertUser(user)).collect(Collectors.toList());
		return userRecords;
	}

	
	public static UserRecord convertUser(final User user) {
		return new UserRecord(user.getId(), user.getFirstName(), user.getLastName(), user.getBirthDate());
	}
}
