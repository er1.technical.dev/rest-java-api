package fr.erdprt.api.record;

import java.sql.Timestamp;

public record LanguageRecord(Integer id, String name, Timestamp lastUpdate) {

}
