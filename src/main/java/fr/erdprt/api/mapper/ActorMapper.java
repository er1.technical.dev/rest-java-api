package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Actor;
import fr.erdprt.api.record.ActorRecord;

@Mapper(componentModel = "spring")
public interface ActorMapper {

	ActorMapper INSTANCE = Mappers.getMapper(ActorMapper.class);

	ActorRecord actorToActorRecord(Actor actor);

	List<ActorRecord> actorToActorRecord(List<Actor> actors);

	Actor actorRecordToActor(ActorRecord actorRecord);

	List<Actor> actorRecordToActor(List<ActorRecord> actorRecords);

}
