package fr.erdprt.api.service;

import java.util.List;

import fr.erdprt.api.model.Inventory;

public interface InventoryService {

	List<Inventory> findAllEager();

	List<Inventory> findByPages(Integer index, Integer size);

	Inventory findById(Integer id);

}