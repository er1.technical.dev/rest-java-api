package fr.erdprt.api.record;

import java.sql.Timestamp;

public record StoreRecord(Integer id, StaffRecord staff, AddressRecord address, Timestamp lastUpdate) {

}
