package fr.erdprt.api.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import fr.erdprt.api.model.Country;
import fr.erdprt.api.model.User;

@DataJpaTest
class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Test
	void findAll() {
		List<User> list = new ArrayList<>();
		this.userRepository.findAll().forEach(list::add);
		Assert.assertEquals(5, list.size());
	}

	@Test
	void findById() {
		Optional<User> user =  this.userRepository.findById(1);
		Assert.assertFalse(user.isEmpty());
		Assert.assertEquals(Integer.valueOf(1), user.get().getId());
	}


	@Test
	void save() throws ParseException {
		Country country = countryRepository.findById(2).get();

		User user = new User("FN", "LN", new SimpleDateFormat("dd-MM-yyyy").parse("12-01-1941"), country);
		this.userRepository.save(user);

		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getId());

		Optional<User> newUser =  this.userRepository.findById(user.getId());
		Assert.assertNotNull(newUser.get());
		Assert.assertEquals(newUser.get().getFirstName(), user.getFirstName());

		List<User> list = new ArrayList<>();
		this.userRepository.findAll().forEach(list::add);
		Assert.assertEquals(6, list.size());
	}

}
