package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.City;
import fr.erdprt.api.record.CityRecord;

@Mapper(componentModel = "spring")
public interface CityMapper {

	CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);

	CityRecord cityToCityRecord(City city);

	List<CityRecord> cityToCityRecord(List<City> cities);

	City cityRecordToCity(CityRecord cityRecord);

	List<City> cityRecordToCity(List<CityRecord> cityRecords);

}
