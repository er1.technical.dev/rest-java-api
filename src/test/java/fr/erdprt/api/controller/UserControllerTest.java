package fr.erdprt.api.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import fr.erdprt.api.Application;

//@Sql(scripts = {"classpath:/sql/schema.sql","classpath:/sql/datas-test.sql"}, executionPhase = ExecutionPhase.BEFORE_TEST_CLASS)
//@Sql(scripts = {"classpath:/sql/drop.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_CLASS)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
class UserControllerTest extends AbstractControllerTest {

    @Autowired
    private MockMvc mvc;

	@Test
	void findAll() throws Exception {

	    mvc.perform(get("/apis/users/all/")
	    	      .contentType(MediaType.APPLICATION_JSON))
	    	      .andExpect(status().isOk())
	    	      .andExpect(content()
	    	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	      .andExpect(jsonPath("$[0].firstName", is("DURAND")))
	    	      .andExpect(jsonPath("$.*", hasSize(5)));
	}

	@Test
	void findById() throws Exception {

	    mvc.perform(get("/apis/users/5/")
	    	      .contentType(MediaType.APPLICATION_JSON))
	    	      .andExpect(status().isOk())
	    	      .andExpect(content()
	    	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	    	      .andExpect(jsonPath("firstName", is("TERAF")));

	}

	@Test
	void save() throws Exception {

		String content = "{ \"country\" : { \"id\": \"1\" }, \"firstName\": \"DIRAND\", \"lastName\": \"JEANNE\", \"birthDate\": \"1991-01-01\"}";

	    mvc.perform(post("/apis/users/")
	    	      .contentType(MediaType.APPLICATION_JSON)
	    	      .content(content)
	    		  .accept(MediaType.APPLICATION_JSON))
	    	      .andExpect(status().isOk());

	}

}
