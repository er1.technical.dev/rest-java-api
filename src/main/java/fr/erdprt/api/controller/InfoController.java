package fr.erdprt.api.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/apis/info", produces = MediaType.APPLICATION_JSON_VALUE)
public class InfoController {

	@GetMapping("/ip")
	public ResponseEntity<String> findHost() throws UnknownHostException {
		String ipAddress = InetAddress.getLocalHost().getHostAddress();
		return new ResponseEntity<>(ipAddress, HttpStatus.OK);
	}

}
