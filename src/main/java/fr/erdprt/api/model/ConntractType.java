package fr.erdprt.api.model;

public enum ConntractType {
	CONTRACT1("contract_1"), CONTRACT2("contract_2"), CONTRACT3("contract_3");
	
	private String contractType;

	private ConntractType(String contractType) {
		this.contractType = contractType;
	}

	public String getContractType() {
		return contractType;
	}
	
	
}
