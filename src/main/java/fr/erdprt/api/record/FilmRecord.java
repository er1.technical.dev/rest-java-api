package fr.erdprt.api.record;

import java.sql.Timestamp;
import java.util.List;

import fr.erdprt.api.model.Language;

public record FilmRecord(Integer id, String title, String description, CategoryRecord category,
		List<ActorRecord> actors, Short releaseYear, Language language, Short rentalDuration, Float rentalRate,
		Short length, Float replacementCost, String rating, String specialFeatures, String fullText,
		Timestamp lastUpdate) {

}
