package fr.erdprt.api.repository;

import org.springframework.data.repository.CrudRepository;

import fr.erdprt.api.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
