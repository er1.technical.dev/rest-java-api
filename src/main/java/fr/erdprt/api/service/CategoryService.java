package fr.erdprt.api.service;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import fr.erdprt.api.model.Category;
import fr.erdprt.api.record.CountryRecord;

public interface CategoryService {

	List<Category> findAll();

	Category findById(Integer id);

}