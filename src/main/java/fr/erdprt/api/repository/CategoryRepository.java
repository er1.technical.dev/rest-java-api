package fr.erdprt.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.erdprt.api.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
