package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Film;
import fr.erdprt.api.record.FilmRecord;

@Mapper(componentModel = "spring")
public interface FilmMapper {

	FilmMapper INSTANCE = Mappers.getMapper(FilmMapper.class);

	FilmRecord filmToFilmRecord(Film film);

	List<FilmRecord> filmToFilmRecord(List<Film> films);

	Film filmRecordToFilm(FilmRecord filmRecord);

	List<Film> filmRecordToFilm(List<FilmRecord> filmRecords);

}
