package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Customer;
import fr.erdprt.api.record.CustomerRecord;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

	CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

	CustomerRecord customerToCustomerRecord(Customer customer);

	List<CustomerRecord> customerToCustomerRecord(List<Customer> customers);

	Customer customerRecordToCustomer(CustomerRecord customerRecord);

	List<Customer> customerRecordToCustomer(List<CustomerRecord> customerRecords);

}
