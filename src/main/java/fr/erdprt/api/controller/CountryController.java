package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.api.record.CountryRecord;
import fr.erdprt.api.service.CountryService;
import fr.erdprt.api.util.CustomErrorType;

@RestController
@RequestMapping(value = "/apis/countries", produces = MediaType.APPLICATION_JSON_VALUE)
public class CountryController {

	public static final Logger logger = LoggerFactory.getLogger(CountryController.class);

	@Autowired
	CountryService countryService;

	// -------------------Retrieve All
	// country---------------------------------------------

	@GetMapping(value = "/all/")
	public ResponseEntity<List<CountryRecord>> findAll() {
		logger.info("Fetching all countries");
		List<CountryRecord> countries = countryService.findAll();
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}

	// -------------------Retrieve like %name%
	// country---------------------------------------------

	@GetMapping(value = "/like/{name}/")
	public ResponseEntity<List<CountryRecord>> like(@PathVariable("name") String name) {
		logger.info("Fetching countries for like %name%");
		List<CountryRecord> countries = countryService.like(name);
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// country------------------------------------------

	@GetMapping(value = "/id/{id}/")
	public ResponseEntity<CountryRecord> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching country with id {}", id);
		CountryRecord country = countryService.findById(id);
		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}
		if (country == null) {
			logger.error("Country with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Country with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(country, HttpStatus.OK);
	}

	// -------------------Create a
	// country-------------------------------------------
	@PostMapping(value = "/")
	public ResponseEntity<CountryRecord> save(@RequestBody CountryRecord country) {
		logger.info("Save country : {}", country);
		return new ResponseEntity<>(country, HttpStatus.OK);
	}

}