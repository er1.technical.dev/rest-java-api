package fr.erdprt.api.record;

import java.sql.Timestamp;

public record CountryNewRecord(Integer id, String name, Timestamp lastUpdate) {

}
