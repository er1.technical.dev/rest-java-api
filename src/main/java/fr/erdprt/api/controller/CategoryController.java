package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.api.mapper.CategoryMapper;
import fr.erdprt.api.model.Category;
import fr.erdprt.api.record.CategoryRecord;
import fr.erdprt.api.service.CategoryService;
import fr.erdprt.api.util.CustomErrorType;

@RestController
@RequestMapping(value = "/apis/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {

	public static final Logger logger = LoggerFactory.getLogger(CountryController.class);

	@Autowired
	CategoryService categoryService;

	// -------------------Retrieve All
	// country---------------------------------------------

	@GetMapping(value = "/all")
	public ResponseEntity<List<CategoryRecord>> findAll() {
		logger.info("Fetching all categories");
		List<Category> categories = categoryService.findAll();
		List<CategoryRecord> categoryRecords = CategoryMapper.INSTANCE.categoryToCategoryRecord(categories);

		return new ResponseEntity<>(categoryRecords, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// country------------------------------------------

	@GetMapping(value = "/id/{id}")
	public ResponseEntity<CategoryRecord> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching country with id {}", id);

		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}

		Category category = categoryService.findById(id);

		if (category == null) {
			logger.error("Country with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("category with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		CategoryRecord categoryRecord = CategoryMapper.INSTANCE.categoryToCategoryRecord(category);

		return new ResponseEntity<>(categoryRecord, HttpStatus.OK);
	}

}
