package fr.erdprt.api.record;

import java.sql.Timestamp;

public record CityRecord(Integer id, String city, CountryRecord country, Timestamp lastUpdate) {

}
