package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.api.mapper.FilmMapper;
import fr.erdprt.api.model.Film;
import fr.erdprt.api.record.FilmRecord;
import fr.erdprt.api.service.FilmService;
import fr.erdprt.api.util.CustomErrorType;

@RestController
@RequestMapping(value = "/apis/films", produces = MediaType.APPLICATION_JSON_VALUE)
public class FilmController {

	public static final Logger logger = LoggerFactory.getLogger(FilmController.class);

	@Autowired
	FilmService filmService;

	// -------------------Retrieve All
	// country---------------------------------------------

	@GetMapping(value = "/all/")
	public ResponseEntity<List<FilmRecord>> findAll() {

		logger.info("Fetching all film");
		List<Film> films = filmService.findAll();
		List<FilmRecord> filmRecords = FilmMapper.INSTANCE.filmToFilmRecord(films);

		return new ResponseEntity<>(filmRecords, HttpStatus.OK);
	}

	@GetMapping(value = "/page")
	public ResponseEntity<List<FilmRecord>> findPage(@RequestParam(name = "index", defaultValue = "0") Integer index,
			@RequestParam(name = "size", defaultValue = "1") Integer size) {

		logger.info("Fetching films page");
		List<Film> films = filmService.findByPages(index, size);
		List<FilmRecord> filmRecords = FilmMapper.INSTANCE.filmToFilmRecord(films);

		return new ResponseEntity<>(filmRecords, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// country------------------------------------------

	@GetMapping(value = "/id/{id}/")
	public ResponseEntity<FilmRecord> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching film with id {}", id);

		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}

		Film film = filmService.findById(id);

		if (film == null) {
			logger.error("Film with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("film with id " + id + " not found"), HttpStatus.NOT_FOUND);
		}
		FilmRecord filmRecord = FilmMapper.INSTANCE.filmToFilmRecord(film);

		return new ResponseEntity<>(filmRecord, HttpStatus.OK);
	}

}
