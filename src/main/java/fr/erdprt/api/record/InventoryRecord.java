package fr.erdprt.api.record;

import java.sql.Timestamp;
import java.util.List;

import fr.erdprt.api.model.Rental;
import fr.erdprt.api.model.Store;

public record InventoryRecord(Integer id, FilmRecord film, Store store, List<Rental> rentals, Timestamp lastUpdate) {

}
