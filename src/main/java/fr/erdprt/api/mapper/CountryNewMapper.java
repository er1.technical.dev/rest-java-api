package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.CountryNew;
import fr.erdprt.api.record.CountryNewRecord;

@Mapper(componentModel = "spring")
public interface CountryNewMapper {

	CountryNewMapper INSTANCE = Mappers.getMapper(CountryNewMapper.class);

	CountryNewRecord countryNewToCountryNewRecord(CountryNew countryNew);

	List<CountryNewRecord> countryNewToCountryNewRecord(List<CountryNew> countryNews);

	CountryNew countryNewRecordToCountryNew(CountryNewRecord countryNewRecord);

	List<CountryNew> countryNewRecordToCountryNew(List<CountryNewRecord> countryNewRecords);

}
