package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Address;
import fr.erdprt.api.model.Store;
import fr.erdprt.api.record.AddressRecord;

@Mapper(componentModel = "spring")
public interface AddressMapper {

	AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

	AddressRecord addressToAddressRecord(Address address);

	List<AddressRecord> addressToAddressRecord(List<Address> addresses);

	Store addressRecordToAddress(AddressRecord addressRecord);

	List<Store> addressRecordToAddress(List<AddressRecord> addressRecords);

}
