package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Payment;
import fr.erdprt.api.record.PaymentRecord;

@Mapper(componentModel = "spring")
public interface PaymentMapper {

	PaymentMapper INSTANCE = Mappers.getMapper(PaymentMapper.class);

	PaymentRecord paymentToPaymentRecord(Payment payment);

	List<PaymentRecord> paymentToPaymentRecord(List<Payment> payments);

	Payment paymentRecordToPayment(PaymentRecord paymentRecord);

	List<Payment> paymentRecordToPayment(List<PaymentRecord> paymentRecords);

}
