package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Category;
import fr.erdprt.api.record.CategoryRecord;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

	CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

	CategoryRecord categoryToCategoryRecord(Category category);

	List<CategoryRecord> categoryToCategoryRecord(List<Category> categories);

	Category categoryRecordToCategory(CategoryRecord categoryRecord);

	List<Category> categoryRecordToCategory(List<CategoryRecord> categoryRecords);

}
