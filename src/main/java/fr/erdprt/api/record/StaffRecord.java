package fr.erdprt.api.record;

import java.util.List;

public record StaffRecord(Integer id, String firstName, String lastName, AddressRecord address,
		List<PaymentRecord> paymentRecords, String email, Integer storeId, Boolean active, String userName,
		String password) {

}
