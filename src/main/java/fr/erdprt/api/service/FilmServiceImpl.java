package fr.erdprt.api.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.erdprt.api.model.Film;
import fr.erdprt.api.repository.FilmRepository;

@CacheConfig(cacheNames = { "film" })
@Service("filmService")
public class FilmServiceImpl implements FilmService {

	public static final Logger logger = LoggerFactory.getLogger(FilmServiceImpl.class);

	@Autowired
	private FilmRepository filmRepository;

	@Override
	@Cacheable()
	public List<Film> findAll() {

		List<Film> films = this.filmRepository.findAllEager();
		return films;
	}

	@Override
	public List<Film> findByPages(final Integer index, final Integer size) {

		Pageable pageRequest = PageRequest.of(index, size, Sort.by("id").ascending());
		Page<Film> page = this.filmRepository.findAll(pageRequest);
		return page.getContent();
	}

	@Override
	@Cacheable(value = "film", key = "#id")
	public Film findById(final Integer id) {

		Optional<Film> film = this.filmRepository.findById(id);
		return film.get();
		/*
		 * if (!film.isEmpty()) { return film.get(); } return null;
		 */
	}

}
