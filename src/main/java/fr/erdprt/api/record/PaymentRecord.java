package fr.erdprt.api.record;

import java.sql.Timestamp;

public record PaymentRecord(Integer id, Float amount, Timestamp paymentDate) {

}
