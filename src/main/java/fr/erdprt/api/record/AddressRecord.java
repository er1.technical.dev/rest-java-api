package fr.erdprt.api.record;

import java.sql.Timestamp;

public record AddressRecord(Integer id, String address1, String address2, String district, CityRecord city,
		Timestamp lastUpdate) {

}
