package fr.erdprt.api.record;

import java.util.Date;

public record UserRecord(Integer id, String firstName, String lastName, Date birthDate, CountryRecord country) {

	public UserRecord(Integer id, String firstName, String lastName, Date birthDate) {
		this(id, firstName, lastName, birthDate, null);
	}
}
