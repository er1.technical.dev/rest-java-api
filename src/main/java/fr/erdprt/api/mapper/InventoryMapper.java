package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Inventory;
import fr.erdprt.api.record.InventoryRecord;

@Mapper(componentModel = "spring")
public interface InventoryMapper {

	InventoryMapper INSTANCE = Mappers.getMapper(InventoryMapper.class);

	InventoryRecord inventoryToInventoryRecord(Inventory inventory);

	List<InventoryRecord> inventoryToInventoryRecord(List<Inventory> inventories);

	Inventory filmRecordToFilm(InventoryRecord inventoryRecord);

	List<Inventory> filmRecordToFilm(List<InventoryRecord> inventoryRecords);

}
