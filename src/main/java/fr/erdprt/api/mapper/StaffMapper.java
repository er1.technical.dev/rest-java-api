package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Staff;
import fr.erdprt.api.record.StaffRecord;

@Mapper(componentModel = "spring")
public interface StaffMapper {

	StaffMapper INSTANCE = Mappers.getMapper(StaffMapper.class);

	StaffRecord staffToStaffRecord(Staff staff);

	List<StaffRecord> staffToStaffRecord(List<Staff> staffs);

	Staff staffRecordToStaff(StaffRecord staffRecord);

	List<Staff> staffRecordToStaff(List<StaffRecord> staffRecords);

}
