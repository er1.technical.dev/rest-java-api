package fr.erdprt.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.erdprt.api.model.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> {

	@Query("select inventory from Inventory inventory LEFT JOIN FETCH inventory.film film LEFT JOIN FETCH film.category LEFT JOIN FETCH film.language LEFT JOIN FETCH inventory.store store LEFT JOIN FETCH store.address address")
	public List<Inventory> findAllEager();

	@Query("select inventory from Inventory inventory LEFT JOIN FETCH inventory.film film LEFT JOIN FETCH film.category LEFT JOIN FETCH film.language LEFT JOIN FETCH inventory.store store LEFT JOIN FETCH store.address address")
	public Page<Inventory> findPages(Pageable page);

	@Query("select inventory from Inventory inventory LEFT JOIN FETCH inventory.film film LEFT JOIN FETCH film.category LEFT JOIN FETCH film.language LEFT JOIN FETCH inventory.store store LEFT JOIN FETCH store.address address where inventory.id=:id")
	public Inventory findByIdEager(Integer id);

}
