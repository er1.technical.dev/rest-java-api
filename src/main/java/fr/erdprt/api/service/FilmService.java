package fr.erdprt.api.service;

import java.util.List;

import fr.erdprt.api.model.Film;

public interface FilmService {

	List<Film> findAll();

	public List<Film> findByPages(final Integer index, final Integer size);

	Film findById(Integer id);

}