package fr.erdprt.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import fr.erdprt.api.model.Store;
import fr.erdprt.api.record.StoreRecord;

@Mapper(componentModel = "spring")
public interface StoreMapper {

	StoreMapper INSTANCE = Mappers.getMapper(StoreMapper.class);

	StoreRecord storeToStoreRecord(Store store);

	List<StoreRecord> storeToStoreRecord(List<Store> stores);

	Store storeRecordToStore(StoreRecord storeRecord);

	List<Store> storeRecordToStore(List<StoreRecord> storeRecords);

}
