package fr.erdprt.api.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import fr.erdprt.api.model.Country;

@DataJpaTest
class CountryRepositoryITTest extends DatabaseTest {

    @Autowired
    private CountryRepository countryRepository;

	@Test
	void findAll() {
		List<Country> list = new ArrayList<>();
		this.countryRepository.findAll().forEach(list::add);
		Assert.assertEquals(239, list.size());
	}

	@Test
	void findById() {
		Optional<Country> country =  this.countryRepository.findById(1);
		Assert.assertFalse(country.isEmpty());
		Assert.assertEquals(Integer.valueOf(1), country.get().getId());
	}

	@Test
	void save() throws ParseException {

		Country country = new Country("Zebuland", "ZE", "Zebu Land", "ZEB", Short.valueOf("23"), Short.valueOf("230"));
		this.countryRepository.save(country);

		Assert.assertNotNull(country);
		Assert.assertNotNull(country.getId());

		Optional<Country> newCountry =  this.countryRepository.findById(country.getId());
		Assert.assertFalse(newCountry.isEmpty());
		Assert.assertEquals(newCountry.get().getName(), country.getName());

		List<Country> list = new ArrayList<>();
		this.countryRepository.findAll().forEach(list::add);
		Assert.assertEquals(240, list.size());

	}

}